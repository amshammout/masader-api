<?php

namespace App\Http\Controllers\ApiControllers;

use App\Http\Controllers\Controller;
use App\Http\Requests\KnowledgeUnitStoreRequest;
use App\Http\Requests\KnowledgeUnitUpdateRequest;
use App\Imports\KuImport;
use App\Models\KnowledgeUnit;
use App\Models\KuImage;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Maatwebsite\Excel\Facades\Excel;

class KnowledgeUnitApiController extends BaseApiController
{
    public function __construct()
    {
        $this->class = KnowledgeUnit::class;
        $this->with = ["accessories.type"];
    }

    protected function beforeIndex(Builder $query)
    {
        $request  = $this->getRequest();
        $request->validate([
            "book_id" => ["required","exists:books,id"]
        ]);

        $book_id = $request->input("book_id");
        $query->where("book_id",$book_id);
//        if (!auth("api")->user()->is_admin)
//            $query->where("publish_state",1);
        $query->select("id","number","header");
        return $query;
    }


    public function store(KnowledgeUnitStoreRequest $request)
    {
        $data = $request->validated();
        $number = 0;
        while ($number == 0) {
            $g_number = rand(100000000, 999999999);
            if (KnowledgeUnit::where('number', $number)->count() == 0)
                $number = $g_number;
        }
        $data["number"] = $number;
        $unit = KnowledgeUnit::create($data);
        foreach ($data["texts"] as $key => $item){
            $unit->accessories()->create([
                "type_id" => $data["type_ids"][$key],
                "text" => $data["texts"][$key]
            ]);
        }

        return $this->sendResponse($unit->fresh());
    }

    public function update(KnowledgeUnitUpdateRequest $request, $id)
    {

        $data = $request->validated();
        $unit = KnowledgeUnit::query()->findOrFail($id);
        $unit->update($data);
        if (isset($data["deleted_accessories"]) && count($data["deleted_accessories"])){
            foreach ($data["deleted_accessories"] as $key => $item){
                $unit->accessories()->find($item)->delete();
            }
        }
        if (isset($data["texts"]) && count($data["texts"])){
            foreach ( $data["texts"] as $key => $item){
                $unit->accessories()->create([
                    "type_id" => $data["type_ids"][$key],
                    "text" => $data["texts"][$key]
                ]);
            }
        }

        return $this->sendResponse($unit->fresh());
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $unit = KnowledgeUnit::query()->findOrFail($id);
        DB::beginTransaction();
        $unit->accessories()->delete();
        $unit->delete();
        DB::commit();
        return  $this->sendResponse();
    }
    public function storeFromExcel(Request $request){
        $request->validate([
            "excel" => "required|mimes:xls,xlsx"
        ]);
        $file = $request->file("excel");
        Excel::import(new KuImport(),$file);
    }
    public function storeImage(Request $request){
        $request->validate([
            "image" => "required"
        ]);
        $path = Storage::disk("public")->put("ku_images",$request->file("image"));
        $path = "/storage/" .$path;
        $image = new KuImage();
        $image->image = $path;
        $image->save();
        return $this->sendResponse($path);
    }
}
