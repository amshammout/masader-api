<?php

namespace App\Http\Controllers\ApiControllers;

use App\Http\Controllers\ApiControllers\BaseApiController;
use App\Http\Requests\BookRequest;
use App\Models;
use App\Models\Book;
use App\Models\Category;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class BookApiController extends BaseApiController
{
    public function __construct()
    {
        $this->class = Book::class;
        $this->with = [
            "categories",
            "publisher",
            "language",
            "cover_type",
            "book_size",
            "copy_type",
            "knowledgeUnits"
            ];
    }

    protected function beforeIndex(Builder $query)
    {
        $request = $this->getRequest();
        if ($request->input("category_id"))
            $query->where("category_id", $request->input("category_id"));
        return $query;
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(BookRequest $request)
    {
        $validatedRequest = $request->validated();
        try {
            DB::beginTransaction();
            $book = new Book($validatedRequest);
            if ($request->file("image") != null) {
                $path = Storage::disk('public')->put('books_covers', $request->file("image"));
                $book->image = "/storage/".$path;
            }
//$category = Category::query()->findOrFail($book->category_id);
//
//if ($category->children->count() > 0)
//abort(422, 'could not add book because parent category have categories');


            $book->save();
            $book->categories()->sync($request->category_ids);
            DB::commit();
        } catch (\Throwable $e) {
            DB::rollback();
            app('log')->error("error during store book", ['exception' => json_encode($e)]);
            throw $e;
        }
        return $this->sendResponse($book->fresh());
    }


    /**
     * Display the specified resource.
     *
     * @param \App\Models\book $book
     * @return \Illuminate\Http\Response
     */
//    public function show(Book $book)
//    {
//        $result = Book::query()->findOrFail($book->id);
//        return $this->sendResponse($result);
//    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\book $book
     * @return \Illuminate\Http\Response
     */
    public function update(BookRequest $request, Book $book)
    {
        $validatedRequest = $request->validated();
        try {
            DB::beginTransaction();
            $book->name = $validatedRequest['name'];
            $book->order = $validatedRequest['order'];
            $book->author = $validatedRequest['author'];
            $book->content_type_number = $validatedRequest['content_type_number'];
            $book->ku_range_number = $validatedRequest['ku_range_number'];
            $book->ku_range = $validatedRequest['ku_range'];
            $book->book_number = $validatedRequest['book_number'];
            $book->book_identity_number = $validatedRequest['book_identity_number'];
            $book->publisher_id = $validatedRequest['publisher_id'];
            $book->edition_number = $validatedRequest['edition_number'];
            $book->language_id = $validatedRequest['language_id'];
            $book->pages_count = $validatedRequest['pages_count'];
            $book->folders_count = $validatedRequest['folders_count'];
            $book->isbn = $validatedRequest['isbn'];
            $book->ikun = $validatedRequest['ikun'];
            $book->cover_type_id = $validatedRequest['cover_type_id'];
            $book->book_size_id = $validatedRequest['book_size_id'];
            $book->publication_year = $validatedRequest['publication_year'];
            $book->copy_type_id = $validatedRequest['copy_type_id'];
            $book->summary = $validatedRequest['summary'];
            if ($request->file("image") != null) {
                $path = Storage::disk('public')->put('books_covers', $request->file("image"));
                if ($book->image != null){
                    Storage::disk('public')->delete(str_replace("/storage","",$book->image));
                }
                $book->image = "/storage/".$path;
            }

            $book->categories()->sync($request->category_ids);
            $book->save();
            DB::commit();
        } catch (\Throwable $e) {
            DB::rollback();
            app('log')->error("error during update book", ['exception' => json_encode($e)]);
            throw $e;
        }
        return $this->sendResponse($book);
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Models\book $book
     * @return \Illuminate\Http\Response
     */
    public function destroy(book $book)
    {
        $book = Book::query()->findOrFail($book->id);
        Book::destroy($book->id);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function getCategoryBooks($categoryId)
    {
        $main_category = Category::query()->findOrFail($categoryId);
        $categoryIds = $main_category->getDescendants()->pluck('id')->toArray();
        array_push($categoryIds, $main_category->id);

        $books = Book::whereHas('categories', function ($query) use ($categoryIds) {
            $query->whereIn('category_id', $categoryIds);
        })->get();


        //    $c = $category->getDescendants()->with('books');

        //$categoryIds = $category->getDescendants()->pluck('id')->toArray();
        // array_push($categoryIds, $categoryId);

        // $books = Book::query()->whereHas('category_id', $categoryIds)->get();

        return $this->sendResponse($books);
    }

    public function addBookCategories(Request $request, $bookId)
    {
        Validator::make($request->all(), [
            'categories_ids' => 'required|array|min:1',
            'categories_ids.*' => 'distinct|exists:category,id'
        ]);
        $book = Book::findOrFail($bookId);
        $book->categories()->sync($request->input('categories_ids'));
    }
}
