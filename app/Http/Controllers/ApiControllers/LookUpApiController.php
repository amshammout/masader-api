<?php

namespace App\Http\Controllers\ApiControllers;

use App\Http\Controllers\ApiControllers\BaseApiController;
use App\Models\BookSize;
use App\Models\CopyType;
use App\Models\CoverType;
use App\Models\Language;
use Illuminate\Http\Request;

class LookUpApiController extends BaseApiController
{
    public function getCopyTypes(Request $request)
    {
        $value = CopyType::get();
        return $this->sendResponse($value);
    }

    public function getCoverTypes(Request $request)
    {
        $value = CoverType::get();
        return $this->sendResponse($value);
    }

    public function getLanguages(Request $request)
    {
        $value = Language::get();
        return $this->sendResponse($value);
    }

    public function getBookSizes(Request $request)
    {
        $value = BookSize::get();
        return $this->sendResponse($value);
    }

}
