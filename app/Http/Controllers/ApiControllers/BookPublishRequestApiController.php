<?php

namespace App\Http\Controllers\ApiControllers;

use App\Models\BookPublishRequest;
use App\Http\Controllers\Controller;
use App\Models\Book;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;

class BookPublishRequestApiController extends BaseApiController
{
    public function __construct()
    {
        $this->class = BookPublishRequest::class;
    }

    protected function beforeIndex(Builder $query)
    {
        $query->where("state",1);
        return $query;
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            "book_id" => "required|exists:books,id"
        ]);
        $r = BookPublishRequest::create([
            "book_id" => $request->input("book_id")
        ]);
        $r->fresh()->book()->update([
            "publish_state" => BookPublishRequest::WAITING
        ]);
        return  $this->sendResponse($r->fresh());
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }



    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            "approved" => "required|boolean"
        ]);
        $book = BookPublishRequest::query()->findOrFail($id)->book;
        $book->publish_state = (boolean) $request->input("approved") ? BookPublishRequest::PUBLISHED : BookPublishRequest::ABORTED;
        $book->save();
        return $this->sendResponse($book->fresh());
    }

}
