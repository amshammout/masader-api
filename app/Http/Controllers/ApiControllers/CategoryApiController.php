<?php

namespace App\Http\Controllers\ApiControllers;

use App\Models\Category;
use App\Http\Requests\CategoryRequest;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use function GuzzleHttp\Psr7\build_query;


class CategoryApiController extends BaseApiController
{
    public function __construct()
    {
        $this->class = Category::class;
    }

    protected function beforeIndex(Builder $query)
    {
        $request = $this->getRequest();
        if ($request->parent_id)
            $query->where("parent_id", $request->parent_id);
        return $query;
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(CategoryRequest $request)
    {
        $validatedRequest = $request->validated();
        try {
            DB::beginTransaction();

            $category = new Category($validatedRequest);

            if ($category->parent != null && $category->parent->books->count() > 0)
                abort(422, 'could not add because parent category have books');

            $category->save();
            DB::commit();

        } catch (\Throwable $e) {
            DB::rollback();
            app('log')->error("error during store category", ['exception' => json_encode($e)]);
            throw $e;
        }
        return $this->sendResponse($category->fresh());
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Models\Category $category
     * @return \Illuminate\Http\Response
     */
    public function show(Category $category)
    {
        $result = Category::query()->findOrFail($category->id);
        return $this->sendResponse($result);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Category $category
     * @return \Illuminate\Http\Response
     */
    public function update(CategoryRequest $request, Category $category)
    {
        $validatedRequest = $request->validated();
        try {
            DB::beginTransaction();
            $category->name = $validatedRequest['name'];

            if ($request->parent_id && $request->parent_id != $category->parent_id)
            {
                //$newCategory = Category::query()->findOrFail( $request->parent_id);
                //if ($newCategory->books->count() > 0)
                $category->parent_id = $validatedRequest['parent_id'];
            }
            if ($request->notes)
                $category->notes = $validatedRequest['notes'];

            $category->save();

            DB::commit();
        } catch (\Throwable $e) {
            DB::rollback();
            app('log')->error("error during update category", ['exception' => json_encode($e)]);
            throw $e;
        }
        return $this->sendResponse($category);
    }


    public function getRelationalCategory()
    {
        $categories = Category::query()->whereNull("parent_id")->with(['parent'])->get();
        return $this->sendResponse($categories);
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Models\Category $category
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $category = Category::query()->findOrFail($id);
        if (count($category->children))
            return $this->sendError("لا يمكنك حذف تصنيف له تصنيفات فرعية", 403);
        Category::destroy($category->id);

        $categories = Category::query()->where("parent_id", $category->parent_id)->get();
        return $this->sendResponse($categories);
    }


}
