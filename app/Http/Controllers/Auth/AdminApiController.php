<?php

namespace App\Http\Controllers\Auth;

use App\Models\Agent;

use App\Models\Country;
use App\Models\MobileToken;
use App\Models\User;
use App\Traits\ApiDecorator;
use Kreait\Laravel\Firebase\Facades\FirebaseMessaging;
use Propaganistas\LaravelPhone\PhoneNumber;


class AdminApiController extends BaseUserApiController
{
    public function __construct()
    {
        $this->phone_required = false;
    }


    protected function afterRegisteringUser(User $user, $data)
    {
        $user->is_admin = true;
        $user->save();
    }

    /**
     * @param $user_id
     * @return User
     */
    protected function getUserWithRelation($user_id)
    {
        $user = User::where('id', $user_id)
            ->first();
        return $user;
    }

    protected function addRoleToRegister()
    {
        return [];
    }

    function subscribeTokens(string $token_type, User $user, $token)
    {
        $topic = "admin";
        FirebaseMessaging::subscribeToTopic($topic, $token);
    }


    function unsubscribeToken($token,$user)
    {
        FirebaseMessaging::unsubscribeFromTopic("admin", $token);
    }


}
