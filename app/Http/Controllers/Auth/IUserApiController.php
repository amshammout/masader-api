<?php


namespace App\Http\Controllers\Auth;


use Illuminate\Http\Request;

interface IUserApiController
{
    public function registerByEmail(Request $request);

    public function registerByPhone(Request $request);

    public function reSendSMSVerificationCode(Request $request);

    public function canSendSMSVerificationCode(Request $request);

    public function reSendMailVerificationCode(Request $request);

    public function loginByPhone(Request $request);

    public function loginByEmail(Request $request);

    public function socialLogin(Request $request, $provider);

    public function callBack_socialLogin(Request $request, $provider);

    public function refreshToken(Request $request);

    public function saveInstanceId(Request $request);

    public function deleteInstanceId(Request $request);

    public function verifyPhone(Request $request);

    public function verifyEmail(Request $request);

    public function profile(Request $request);

    public function saveLocale(Request $request);

    public function forgetByEmail(Request $request);

    public function forgetByPhone(Request $request);

    public function resetByEmail(Request $request);

    public function resetByPhone(Request $request);

    public function update(Request $request);
}
