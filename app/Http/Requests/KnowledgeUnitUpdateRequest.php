<?php

namespace App\Http\Requests;

use App\Models\KnowledgeUnit;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class KnowledgeUnitUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id = $this->route("knowledge_unit");
        $types_validate_role = Rule::unique("ku_accessories","type_id")->where("knowledge_unit_id",$id);
        if ($this->deleted_accessories && count($this->deleted_accessories)){
            $currentAccessoriesIds = KnowledgeUnit::query()->findOrFail($id)->accessories()->pluck("id")->toArray();
            $notAllowedTypes = array_diff($currentAccessoriesIds,$this->deleted_accessories);
            $types_validate_role = Rule::notIn($notAllowedTypes);
        }


        return [
            "header" => "required|string|min:20|max:200",
            "body" => "required|string|min:100",
            "type_ids" => ["nullable","array",$this->type_ids ? "size:". count($this->texts) : null],
            "type_ids.*" => [
                "exists:ku_accessory_types,id",
                "distinct",
                $types_validate_role

            ],
            "texts" => ["nullable","array"],
            "texts.*" => "string|min:20",
            "deleted_accessories" => "array|nullable|distinct",
            "deleted_accessories.*" => "exists:ku_accessories,id"
        ];
    }
}
