<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class KnowledgeUnitStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "header" => "required|string|min:20|max:200",
            "body" => "required|string|min:100",
            "book_id" => "required|exists:books,id",
            "type_ids" => ["required","array",$this->type_ids ? "size:". count($this->texts) : null],
            "type_ids.*" => "exists:ku_accessory_types,id|distinct",
            "texts" => ["required","array"],
            "texts.*" => "string|min:20"
        ];
    }
}
