<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class BookRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|string',
            'order' => 'nullable|numeric',
            'author' => 'required|string',
            'content_type_number' => 'required|numeric',
            'ku_range_number' => 'required|numeric',
            'ku_range' => 'required|numeric',
            'book_number' => 'required|numeric',
            'book_identity_number' => 'required|numeric',
            'publisher_id' => 'required|exists:publishers,id',
            'edition_number' => 'required|numeric',
            'language_id' => 'required|exists:languages,id',
            'pages_count' => 'nullable|numeric',
            'folders_count' => 'nullable|numeric',
            'isbn' => 'nullable|numeric',
            'ikun' => 'nullable|numeric',
            'cover_type_id' => 'nullable|exists:cover_types,id',
            'book_size_id' => 'nullable|exists:book_sizes,id',
            'publication_year' => 'nullable|numeric',
            'copy_type_id' => 'nullable|exists:copy_types,id',
            'image' => 'nullable',
            'summary' => 'nullable|string',
            'category_ids' => 'array|required',
            'category_ids.*' => 'exists:categories,id',
        ];
    }

}
