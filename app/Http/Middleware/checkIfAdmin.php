<?php

namespace App\Http\Middleware;


use Closure;
use Illuminate\Http\Request;

class checkIfAdmin
{


    public function handle($request, Closure $next)
    {
        if (!$request->user()->is_admin){
            return abort(403,"Permission Denied");
        }
        return $next($request);
    }
}
