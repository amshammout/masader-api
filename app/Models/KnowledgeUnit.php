<?php

namespace App\Models;

use App\Models\Book;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\KnowledgeUnit
 *
 * @property int $id
 * @property string $number
 * @property string $header
 * @property string $body
 * @property int $book_id
 * @property int $publish_state
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\KuAccessory[] $accessories
 * @property-read int|null $accessories_count
 * @property-read \App\Models\Book $book
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KnowledgeUnit newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KnowledgeUnit newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KnowledgeUnit query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KnowledgeUnit whereBody($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KnowledgeUnit whereBookId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KnowledgeUnit whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KnowledgeUnit whereHeader($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KnowledgeUnit whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KnowledgeUnit whereNumber($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KnowledgeUnit wherePublishState($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KnowledgeUnit whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class KnowledgeUnit extends Model
{
    protected $table = "knowledge_units";
    protected $fillable = ["header","body","book_id","number"];
    protected $with = ["accessories"];

    public function accessories(){
        return $this->hasMany(KuAccessory::class);
    }
    public function book(){
        return $this->belongsTo(Book::class);
    }
}
