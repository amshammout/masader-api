<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\Translatable\HasTranslations;

/**
 * App\Models\CoverType
 *
 * @property int $id
 * @property array $name
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read array $translations
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CoverType newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CoverType newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CoverType query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CoverType whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CoverType whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CoverType whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CoverType whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class CoverType extends Model
{
    use HasTranslations;
    public $translatable = ['name'];
    protected $table = 'cover_types';
    protected $fillable = ["name"];
}
