<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\KuAccessory
 *
 * @property int $id
 * @property int $type_id
 * @property string $text
 * @property int $knowledge_unit_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\KnowledgeUnit $knowledgeUnit
 * @property-read \App\Models\KuAccessoryType $type
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KuAccessory newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KuAccessory newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KuAccessory query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KuAccessory whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KuAccessory whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KuAccessory whereKnowledgeUnitId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KuAccessory whereText($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KuAccessory whereTypeId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KuAccessory whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class KuAccessory extends Model
{
    protected $table = "ku_accessories";
    protected $fillable = [
        "text",
        "knowledge_unit_id",
        "type_id"
    ];
    protected $with = ["type"];

    public function knowledgeUnit(){
        return $this->belongsTo(KnowledgeUnit::class);
    }
    public function type(){
        return $this->belongsTo(KuAccessoryType::class,"type_id");
    }

}
