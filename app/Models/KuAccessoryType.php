<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\Translatable\HasTranslations;

/**
 * App\Models\KuAccessoryType
 *
 * @property int $id
 * @property array $name
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read array $translations
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KuAccessoryType newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KuAccessoryType newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KuAccessoryType query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KuAccessoryType whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KuAccessoryType whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KuAccessoryType whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KuAccessoryType whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class KuAccessoryType extends Model
{
    use HasTranslations;
    public $translatable = ['name'];
}
