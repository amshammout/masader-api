<?php

namespace App\Models;

use App\Models\KnowledgeUnit;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Book
 *
 * @property int $id
 * @property string $name
 * @property int|null $order
 * @property string $author
 * @property int $content_type_number
 * @property int $ku_range_number
 * @property int $ku_range
 * @property int $book_number
 * @property int $book_identity_number
 * @property int $publisher_id
 * @property int $edition_number
 * @property int $language_id
 * @property int|null $pages_count
 * @property int|null $folders_count
 * @property int|null $isbn
 * @property int|null $ikun
 * @property int|null $cover_type_id
 * @property int|null $book_size_id
 * @property int|null $publication_year
 * @property int|null $copy_type_id
 * @property string|null $image
 * @property string|null $summary
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\BookSize|null $book_size
 * @property-read \Franzose\ClosureTable\Extensions\Collection|\App\Models\Category[] $categories
 * @property-read int|null $categories_count
 * @property-read \App\Models\CopyType|null $copy_type
 * @property-read \App\Models\CoverType|null $cover_type
 * @property-read \App\Models\Language $language
 * @property-read \App\Models\Publisher $publisher
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Book newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Book newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Book query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Book whereAuthor($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Book whereBookIdentityNumber($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Book whereBookNumber($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Book whereBookSizeId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Book whereContentTypeNumber($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Book whereCopyTypeId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Book whereCoverTypeId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Book whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Book whereEditionNumber($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Book whereFoldersCount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Book whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Book whereIkun($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Book whereImage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Book whereIsbn($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Book whereKuRange($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Book whereKuRangeNumber($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Book whereLanguageId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Book whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Book whereOrder($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Book wherePagesCount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Book wherePublicationYear($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Book wherePublisherId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Book whereSummary($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Book whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property int $publish_state
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\KnowledgeUnit[] $knowledgeUnits
 * @property-read int|null $knowledge_units_count
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Book wherePublishState($value)
 */
class Book extends Model
{

    protected $table = 'books';
    protected $fillable = [
        "name",
        "order",
        "author",
        "category_id",
        "content_type_number",
        "ku_range_number",
        "ku_range",
        "book_number",
        "book_identity_number",
        "publisher_id",
        "edition_number",
        "language_id",
        "pages_count",
        "folders_count",
        "isbn",
        "ikun",
        "cover_type_id",
        "book_size_id",
        "publication_year",
        "copy_type_id",
        "image",
        "summary",
    ];




    public function categories()
    {
        return $this->belongsToMany(Category::class, 'book_category', 'book_id','category_id' );
    }

    public function publisher()
    {
        return $this->belongsTo(Publisher::class, "publisher_id");
    }

    public function language()
    {
        return $this->belongsTo(Language::class, "language_id");

    }

    public function cover_type()
    {
        return $this->belongsTo(CoverType::class, "cover_type_id");

    }

    public function book_size()
    {
        return $this->belongsTo(BookSize::class, "book_size_id");

    }

    public function copy_type()
    {
        return $this->belongsTo(CopyType::class, "copy_type_id");

    }
    public function knowledgeUnits()
    {
        return $this->hasMany(KnowledgeUnit::class);

    }
}
