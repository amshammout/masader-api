<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\Translatable\HasTranslations;

/**
 * App\Models\CopyType
 *
 * @property int $id
 * @property array $name
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read array $translations
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CopyType newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CopyType newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CopyType query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CopyType whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CopyType whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CopyType whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CopyType whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class CopyType extends Model
{
    use HasTranslations;
    public $translatable = ['name'];
    protected $table = 'copy_types';
    protected $fillable = ["name"];
}
