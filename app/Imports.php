<?php

namespace App\Imports;

use App\Models\KnowledgeUnit;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class KuImport implements ToModel,WithHeadingRow
{
    /**
     * @param array $row
     *
     * @return KnowledgeUnit|null
     */
    public function model(array $row)
    {
        return new KnowledgeUnit([
            'header'     => $row["header"],
            'body'    => $row["body"],
            'book_id' => $row["book_id"],
            'number' => $this->getNumber()
        ]);
    }

    private function getNumber()
    {
        $number = 0;
        while ($number == 0) {
            $g_number = rand(100000000, 999999999);
            if (KnowledgeUnit::where('number', $number)->count() == 0)
                $number = $g_number;
        }
        return $number;
    }
}
