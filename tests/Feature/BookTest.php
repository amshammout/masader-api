<?php

namespace Tests\Feature;

use App\Models\Book;
use App\Models\Category;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Facades\App;
use Tests\TestCase;


use App\Http\Controllers\ApiControllers\BaseApiController;
use App\Http\Requests\BookRequest;
use App\Models;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class BookTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    /** @test */
    public function testBookStore()
    {
        app::setLocale('ar');

        $book = factory(Book::class)->make();
        $response = $this->post('api/book', $book->toArray());
        $response->assertSuccessful();

        $bookObj = Book::query()->firstWhere('id', $response['data']['id']);
        $this->assertNotNull($bookObj);
    }

    /** @test */
    public function testcategoryBooks()
    {
        $CategoryObj = factory(Category::class)->create(["name" => 'test']) ; // Category::query()->firstWhere('id', $response['data']['id']);

        $CategoryObj1 = factory(Category::class)->create( ["name" => 'test1', "parent_id" => $CategoryObj->id]);
        $CategoryObj2 = factory(Category::class)->create( ["name" => 'test2', "parent_id" => $CategoryObj->id]);

        $CategoryObj21 =  factory(Category::class)->create( ["name" => 'test2-1', "parent_id" => $CategoryObj2->id]);
        $CategoryObj22 =  factory(Category::class)->create( ["name" => 'test2-2', "parent_id" => $CategoryObj2->id]);

        $book1 = factory(Book::class)->create();//['category_id' => $CategoryObj21->id]
        $book2 = factory(Book::class)->create();//['category_id' => $CategoryObj21->id]
        $CategoryObj21->books()->sync([$book1->id,$book2->id]);
        //
            //$book->categories()->sync($request->input('categories_ids'));

        $book3 = factory(Book::class)->create();//['category_id' => $CategoryObj22->id]
        $book4 = factory(Book::class)->create();//['category_id' => $CategoryObj22->id]

       $CategoryObj22->books()->sync([$book3->id,$book4->id]);

        $CategoryObj->books()->sync([$book3->id,$book4->id]);

        $response = $this->json('get', 'api/categoryBooks/' . $CategoryObj->id);

        $response->assertJsonCount(4, 'data');
    }

    public function testaddBookCategories()
    {

        $collection = factory(Category::class, 3)->create(["name" => 'test']);
        $book1 = factory(Book::class)->create(); //['category_id' => $CategoryObj21->id]


        $p = $this->post('api/addBookCategories/' . $book1->id, ['categories_ids' => $collection->pluck('id')]);
        $p->assertSuccessful();
        $book1 = Book::find($book1->id);
        $ids = $book1->categories()->pluck('id');
        $this->assertCount(3, $ids);
    }

    /** @test */
    public function storeBookWithCategoriesTest(){
        $category = factory(Category::class)->create(["name" => 'test']);
        $book = factory(Book::class)->make()->toArray();
        $book["category_ids"] = [$category->id];

        $response = $this->post("api/book",$book);
        $response->assertSuccessful();
        $added_book = Book::query()->find($response["data"]["id"]);
        $this->assertCount(1,$added_book->categories);
        $this->assertEquals($added_book->categories[0]->id,$category->id);
    }
    /** @test */
    public function updateBookWithCategoriesTest(){
        $category1 = factory(Category::class)->create(["name" => 'test1']);
        $category2 = factory(Category::class)->create(["name" => 'test2']);
        $category3 = factory(Category::class)->create(["name" => 'test3']);
        $book = factory(Book::class)->make()->toArray();
        $book["category_ids"] = [$category1->id];

        $response = $this->post("api/book",$book);
        $response->assertSuccessful();
        $added_book = Book::query()->find($response["data"]["id"]);
        $this->assertCount(1,$added_book->categories);
        $this->assertEquals($added_book->categories[0]->id,$category1->id);


        $book["category_ids"] = [$category2->id,$category3->id];
        $response = $this->put("api/book/$added_book->id",$book,[
            "Accept" => 'application/json'
        ]);
        $response->assertSuccessful();
        $added_book = Book::query()->find($response["data"]["id"]);
        $this->assertCount(2,$added_book->categories);
        $this->assertContains($category3->id,$added_book->categories()->pluck("id")->toArray());
        $this->assertContains($category2->id,$added_book->categories()->pluck("id")->toArray());

        $this->assertNotContains($category1->id,$added_book->categories()->pluck("id")->toArray());
    }


}
