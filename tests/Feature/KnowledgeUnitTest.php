<?php

namespace Tests\Feature;


use App\Models\KnowledgeUnit;
use App\Models\KuAccessory;
use App\Models\KuAccessoryType;
use App\Models\Book;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Str;
use Tests\TestCase;

class KnowledgeUnitTest extends TestCase
{
    use DatabaseTransactions;
    /**
     * A basic test example.
     *
     * @return void
     */
    /** @test */
    public function testKnowledgeUnitStoreWithAccessories()
    {
        app::setLocale('ar');
//        Test 1 : success to add KnowledgeUnit and see it with book show
        $book = factory(Book::class)->create();
        $type1 = factory(KuAccessoryType::class)->create();
        $type2 = factory(KuAccessoryType::class)->create();
        $type3 = factory(KuAccessoryType::class)->create();
        $kuData1 = [
            "header" => Str::random(20),
            "body" => Str::random(100),
            "book_id" => $book->id,
            "texts" => [
                Str::random(35),
                Str::random(35),
                Str::random(35)
            ],
            "type_ids" => [$type1->id,$type2->id,$type3->id]
        ];
        $response1 = $this->postJson("/api/knowledge_unit",$kuData1);
        $response1->assertSuccessful();
        $freshBook = $this->get("/api/book/$book->id");
        $freshBook->assertSee($kuData1["header"]);
//        test if accessories count equal to texts added count
        $this->assertCount(count($kuData1["texts"]),$freshBook["data"]["knowledge_units"][0]["accessories"]);
//        Test 2 : fail to add KnowledgeUnit because duplicate accessory type_id, fail to see with book show
        $kuData2 = [
            "header" =>  Str::random(20),
            "body" => Str::random(100),
            "book_id" => $book->id,
            "texts" => [
                Str::random(35),
                Str::random(35),
                Str::random(35)
            ],
            "type_ids" => [$type1->id,$type2->id,$type2->id]
        ];
        $response2 = $this->postJson("/api/knowledge_unit",$kuData2);
        $response2->assertSee("errors");
        $response2->assertSee("type_ids.2");
        $freshBook2 = $this->get("/api/book/$book->id");
        $freshBook2->assertDontSee($kuData2["header"]);
//        Test 3 : fail to add KnowledgeUnit because texts array size not equal to type_ids array size, fail to see with book show
        $kuData3 = [
            "header" =>  Str::random(20),
            "body" => Str::random(100),
            "book_id" => $book->id,
            "texts" => [
                Str::random(35),
                Str::random(35),
                Str::random(35)
            ],
            "type_ids" => [$type1->id,$type2->id]
        ];
        $response3 = $this->postJson("/api/knowledge_unit",$kuData3);
        $response3->assertSee("errors");
        $response3->assertSee("type_ids");
        $freshBook3 = $this->get("/api/book/$book->id");
        $freshBook3->assertDontSee($kuData3["header"]);
    }
    /** @test */
    public function testUpdateKnowledgeUnitWithAccessories(){
//        Test to update the knowledge accessories
        $accessory1 = factory(KuAccessory::class)->create();
        $unit1 = $accessory1->knowledgeUnit;
        $type1 = factory(KuAccessoryType::class)->create();
        $type2 = factory(KuAccessoryType::class)->create();
        $unit1Data = [
            "header" =>  Str::random(20),
            "body" => Str::random(100),
            "texts" => [
                Str::random(35),
                Str::random(35),
            ],
          "type_ids"  =>  [
                    $type1->id,$type2->id
                ]
        ];
        $response1 = $this->putJson("/api/knowledge_unit/$unit1->id",$unit1Data);
        $response1->assertSuccessful();
        $response1->assertSee($unit1Data["texts"][0]);
        $response1->assertSee($unit1Data["header"]);
//        Test to update the knowledge accessories with exists types but delete old
        $unit1Data["deleted_accessories"] = collect($response1["data"]["accessories"])->pluck("id");
        $response2 = $this->putJson("/api/knowledge_unit/$unit1->id",$unit1Data);
        $response2->assertSuccessful();
        $response2->assertSee($unit1Data["texts"][0]);
        $response2->assertSee($unit1Data["header"]);
//        Test to update the knowledge accessories with exists types without delete old
        $unit1Data["deleted_accessories"] = [];
        $response2 = $this->putJson("/api/knowledge_unit/$unit1->id",$unit1Data);
        $response2->assertSee("errors");
        $response2->assertSee("type_ids");
    }

    /** @test */
    public function testDestroyKnowledgeUnitWithAccessories(){
        $unit = factory(KuAccessory::class)->create()->knowledgeUnit;
        $accessories = KuAccessory::query()->where("knowledge_unit_id",$unit->id)->get();
        $this->assertCount($unit->accessories()->count(),$accessories);
        $response = $this->delete("/api/knowledge_unit/$unit->id");
        $response->assertSuccessful();
        $accessories = KuAccessory::query()->where("knowledge_unit_id",$unit->id)->get();
        $this->assertCount(0,$accessories);
        $unit = KnowledgeUnit::query()->find($unit->id);
        $this->assertNull($unit);

    }

}
