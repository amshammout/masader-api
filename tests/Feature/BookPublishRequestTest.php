<?php

namespace Tests\Feature;


use App\Models\Book;
use App\Models\BookPublishRequest;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;

class BookPublishRequestTest extends TestCase
{
    use DatabaseTransactions;
    /**
     * A basic test example.
     *
     * @return void
     */
    /** @test */
    public function bookPublishRequestStoreTest(){
        $book = factory(Book::class)->create();
        $response = $this->postJson("/api/book_publish_request",[
            "book_id" => $book->id
        ]);
        $response->assertSuccessful();
        $response->assertJsonPath("data.book.id",$book->id);
    }
    /** @test */
    public function bookPublishRequestAcceptanceTest(){
        $book1 = factory(Book::class)->create();
        $this->assertFalse( (boolean) $book1->fresh()->publish_state);
        $request1 = $this->postJson("/api/book_publish_request",[
            "book_id" => $book1->id
        ]);
        $request1->assertSuccessful();
        $request1->assertJsonPath("data.book.publish_state",BookPublishRequest::WAITING);
        $id = $request1["data"]["id"];
        $response = $this->putJson("/api/book_publish_request/$id",[
            "approved" => true
        ]);
        $response->assertSuccessful();
        $response->assertJsonPath("data.publish_state",BookPublishRequest::PUBLISHED);


        $book2 = factory(Book::class)->create();
        $this->assertFalse( (boolean) $book2->fresh()->publish_state);
        $request2 = $this->postJson("/api/book_publish_request",[
            "book_id" => $book2->id
        ]);
        $request2->assertSuccessful();
        $request2->assertJsonPath("data.book.publish_state",BookPublishRequest::WAITING);
        $id = $request2["data"]["id"];
        $response = $this->putJson("/api/book_publish_request/$id",[
            "approved" => false
        ]);
        $response->assertSuccessful();
        $response->assertJsonPath("data.publish_state",BookPublishRequest::ABORTED);
    }

}
