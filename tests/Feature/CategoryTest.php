<?php

namespace Tests\Feature;

use App\Models\Book;
use App\Models\Category;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Facades\App;
use Illuminate\Validation\ValidationException;
use Tests\TestCase;

class CategoryTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    /** @test */
    public function testCategoryStore()
    {
        app::setLocale('ar');

        $response = $this->post('api/category', ["name"=> 'test', "notes" => 'test']);
        $response->assertSuccessful();
        $CategoryObj = Category::query()->firstWhere('id', $response['data']['id']);
        $this->assertNotNull($CategoryObj);
    }

    /** @test */
    public function testUnStoredCategory()
    {
        $result = $this->json('post', 'api/category',  ["name"=> 'test', "notes" => 'test','parent_id' => -1255]);
        $result->assertStatus(422);
    }

    /** @test */
    public function testTranslationCategory()
    {
        App::setLocale('ar');
        $response = $this->post('api/category', ["name"=> 'test', "notes" => 'test']);
        $response->assertSuccessful();

        $CategoryObj = Category::query()->firstWhere('id', $response['data']['id']);
        $this->assertNotNull($CategoryObj);
    }

    /** @test */
    public function testRelationalCategory()
    {
        $response =  $this->get('api/relationalCategory');
        $response->assertSuccessful();
    }

    /** @test */
    public function testDestroyCategory()
    {
        app::setLocale('ar');

        $response = $this->post('api/category', ["name"=> 'test', "notes" => 'test']);
        $response->assertSuccessful();
        $CategoryObj = Category::query()->firstWhere('id', $response['data']['id']);
        $this->assertNotNull($CategoryObj);

        $response =   $this->delete('api/category/'. $CategoryObj->id);
        $response->assertSuccessful();
    }
    /** @test */
    public function testShowCategory()
    {
        app::setLocale('ar');

        $response = $this->post('api/category', ["name"=> 'test', "notes" => 'test']);
        $response->assertSuccessful();

        $CategoryObj = Category::query()->firstWhere('id', $response['data']['id']);
        $this->assertNotNull($CategoryObj);

        $response = $this->get('api/category/'.$CategoryObj->id);
        $response->assertSuccessful();
    }

//    /** @test */
//    public function testAddBookToRootCategoryStore()
//    {
//        app::setLocale('ar');
//
//        $response = $this->post('api/category', ["name"=> 'test', "notes" => 'test']);
//        $CategoryObj = Category::query()->firstWhere('id', $response['data']['id']);
//
//        $response1 = $this->post('api/category', ["name"=> 'test1','parent_id'=> $CategoryObj->id]);
//
//        $book1 = factory(Book::class)->make(['category_id' => $CategoryObj->id]);
//        $responseb1 = $this->json('post','api/book', $book1->toArray());
//
//        $responseb1->assertStatus(422);
//
//    }
//
//    /** @test */
//    public function testAddCategoryToFinalCategoryStore()
//    {
//        app::setLocale('ar');
//
//        $response = $this->post('api/category', ["name"=> 'test', "notes" => 'test']);
//        $CategoryObj = Category::query()->firstWhere('id', $response['data']['id']);
//
//        $response1 = $this->post('api/category', ["name"=> 'test1','parent_id'=> $CategoryObj->id]);
//        $CategoryObj1 = Category::query()->firstWhere('id', $response1['data']['id']);
//
//        $book1 = factory(Book::class)->make(['category_id' => $CategoryObj1->id]);
//        $responseb = $this->json('post','api/book', $book1->toArray());
//
//        $response2 = $this->post('api/category', ["name"=> 'test1','parent_id'=> $CategoryObj1->id]);
//        //$CategoryObj1 = Category::query()->firstWhere('id', $response2['data']['id']);
//
//        $response2->assertStatus(422);
//
//    }
}
