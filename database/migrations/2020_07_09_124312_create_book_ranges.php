<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBookRanges extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('book_type', function (Blueprint $table) {
            $table->id();
            $table->string("name");
            $table->timestamps();
        });

        Schema::create('book_ranges', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger("book_type_id");
            $table->unsignedInteger("order");
            $table->string("number");
            $table->string("type");
            $table->foreign('book_type_id')->references('id')->on('book_type');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('book_ranges');
    }
}
