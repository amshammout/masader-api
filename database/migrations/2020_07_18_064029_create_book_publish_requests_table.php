<?php

use App\Models\BookPublishRequest;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBookPublishRequestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('book_publish_requests', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger("book_id");
            $table->tinyInteger("state")->default(BookPublishRequest::WAITING);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('book_publish_requests');
    }
}
