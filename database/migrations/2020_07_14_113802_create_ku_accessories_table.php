<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateKuAccessoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ku_accessories', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger("type_id");
            $table->text("text");
            $table->unsignedBigInteger("knowledge_unit_id");
            $table->foreign("type_id")->references("id")->on("ku_accessory_types");
            $table->foreign("knowledge_unit_id")->references("id")->on("knowledge_units");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ku_accessories');
    }
}
