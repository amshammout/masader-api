<?php


use App\Models\Category;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;
use App\Models\CategoryClosure;
use Spatie\Translatable\HasTranslations;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        app::setLocale('ar');

        $category = new Category(['name' => "المعارف العامة", 'notes' => ""]);
        $category->setTranslation('name', 'en', 'GENERALITIES');
        $category->save();

        $category = new Category(['name' => "الفلسفة وعلم النفس", 'notes' => ""]);
        $category->setTranslation('name', 'en', 'PHILOSOPHY');
        $category->save();

        $category1 = new Category(['name' => "الديانات", 'notes' => ""]);
        $category1->setTranslation('name', 'en', 'RELIGIONS');
        $category1->save();

        $category = new Category(['name' => "العلوم الاجتماعية", 'notes' => ""]);
        $category->setTranslation('name', 'en', 'SOCIAL SCEINCE');
        $category->save();

        $category = new Category(['name' => "اللّغات", 'notes' => ""]);
        $category->setTranslation('name', 'en', 'LANGUAGES');
        $category->save();

        $category = new Category(['name' => "العلوم البحتة", 'notes' => ""]);
        $category->setTranslation('name', 'en', 'PURE SCEINCES');
        $category->save();

        $category = new Category(['name' => "العلوم التطبيقية", 'notes' => ""]);
        $category->setTranslation('name', 'en', 'APPLIED SCIENCES');
        $category->save();

        $category = new Category(['name' => "الفنون والاستجمام والديكور", 'notes' => ""]);
        $category->setTranslation('name', 'en', 'FINE ARTS');
        $category->save();

        $category = new Category(['name' => "الآداب", 'notes' => ""]);
        $category->setTranslation('name', 'en', 'LITERATURES');
        $category->save();

        $category = new Category(['name' => "التاريخ، الجغرافيا والتراجم", 'notes' => ""]);
        $category->setTranslation('name', 'en', 'HISTORY, GENERAL GEOGRAPHY etc');
        $category->save();

        $id = $category1->id; // DB::table('categories')->where('name->ar', 'الديانات')->get()->first()->id;

        $category = new Category(['name' => "الإسلام وعلومه", 'notes' => "", 'parent_id' => $id]);
        $category->setTranslation('name', 'en', 'Islam and its Sciences');
        $category->save();

        $category = new Category(['name' => "القرآن الكريم وعلومه", 'notes' => "", 'parent_id' => $id]);
        $category->setTranslation('name', 'en', 'The Holy Quran and its Sciences');
        $category->save();

        $category = new Category(['name' => "الحديث الشريف وعلومه", 'notes' => "", 'parent_id' => $id]);
        $category->setTranslation('name', 'en', 'Hadith and its Sciences');
        $category->save();

        $category = new Category(['name' => "العقيدة الإسلامية", 'notes' => "", 'parent_id' => $id]);
        $category->setTranslation('name', 'en', 'Islamic faith');
        $category->save();

        $category = new Category(['name' => "الفرق الإسلامية", 'notes' => "", 'parent_id' => $id]);
        $category->setTranslation('name', 'en', 'Islamic teams');
        $category->save();

        $category = new Category(['name' => "الفقه الإسلامي", 'notes' => "", 'parent_id' => $id]);
        $category->setTranslation('name', 'en', 'Islamic jurisprudence');
        $category->save();

        $category = new Category(['name' => "المذاهب الفقهية الإسلامية", 'notes' => "", 'parent_id' => $id]);
        $category->setTranslation('name', 'en', ' Islamic schools of jurisprudence');
        $category->save();

        $category = new Category(['name' => "الدفاع عن الإسلام، الأحزاب والحركات الإسلامية", 'notes' => "", 'parent_id' => $id]);
        $category->setTranslation('name', 'en', 'Defending Islam, which Islamic movements have');
        $category->save();

        $categoryx = new Category(['name' => "الديانات الأخرى", 'notes' => "", 'parent_id' => $id]);
        $categoryx->setTranslation('name', 'en', 'other religions');
        $categoryx->save();


        $categoryx = new Category(['name' => "ديانة 1", 'notes' => "", 'parent_id' => $categoryx->id]);
        $categoryx->setTranslation('name', 'en', 'religion 1');
        $categoryx->save();

        $categoryx = new Category(['name' => "ديانة 2", 'notes' => "", 'parent_id' => $categoryx->id]);
        $categoryx->setTranslation('name', 'en', 'religion 2');
        $categoryx->save();

        $categoryx = new Category(['name' => "ديانة 3", 'notes' => "", 'parent_id' => $categoryx->id]);
        $categoryx->setTranslation('name', 'en', 'religion 3');
        $categoryx->save();

    }
}
