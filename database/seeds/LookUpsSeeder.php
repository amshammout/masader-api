<?php

use App\Models\BookSize;
use App\Models\CopyType;
use App\Models\CoverType;
use App\Models\Language;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\App;

class LookUpsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        app::setLocale('ar');

        $copyType = new CopyType(['name' => "رقمي"]);
        $copyType->setTranslation('name', 'en', 'Digital');
        $copyType->save();

        $copyType = new CopyType(['name' => "ورقي"]);
        $copyType->setTranslation('name', 'en', 'Paper');
        $copyType->save();

        $copyType = new CopyType(['name' => "رقمي وورقي"]);
        $copyType->setTranslation('name', 'en', 'Paper and Digital');
        $copyType->save();

        $coverType = new CoverType(['name' => "مجلد"]);
        $coverType->setTranslation('name', 'en', 'covered');
        $coverType->save();

        $coverType = new CoverType(['name' => "عادي"]);
        $coverType->setTranslation('name', 'en', 'normal');
        $coverType->save();

        $bookSize = new BookSize(['name' => "كبير"]);
        $bookSize->setTranslation('name', 'en', 'big');
        $bookSize->save();

        $bookSize = new BookSize(['name' => "صغير"]);
        $bookSize->setTranslation('name', 'en', 'small');
        $bookSize->save();


        $language = new Language(['name' => "العربية"]);
        $language->save();

        $language = new Language(['name' => "English"]);
        $language->save();
    }
}
