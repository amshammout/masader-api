<?php

use App\Models\Book;
use App\Models\BookSize;
use App\Models\Category;
use App\Models\CopyType;
use App\Models\CoverType;
use App\Models\Language;
use App\Models\Publisher;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class BookSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //  $id = DB::table('book_type')->where('name' ,'masader')->first()->id;

        DB::table('books')->insert([
            'name' => "كتاب 1",
            'order' => "1",
            'author' => "محمود",
            'content_type_number' => "01",
            //   'ku_range_number' => $id,
            'ku_range' => "01",
            'book_number' => "01",
            'book_identity_number' => "1010000",
            'publisher_id' =>  Publisher::query()->Where('name->ar','دار الفكر')->first()->id,
            'edition_number' => "1",
            'language_id' => Language::query()->Where('name','العربية')->first()->id,
            'pages_count' => "100",
            'folders_count' => "100",
            'isbn' => "123456789012",
            'ikun' => "123456789012",
            'cover_type_id' => CoverType::query()->Where('name->ar','مجلد')->first()->id,
            'book_size_id' =>  BookSize::query()->Where('name->ar','كبير')->first()->id,
            'publication_year' => "2020",
            'copy_type_id' => CopyType::query()->Where('name->ar','ورقي')->first()->id,
            'image' => "/bookUploades",
            'summary' => "مختصر",
        ]);

        DB::table('book_category')->insert([
            'category_id' =>  Category::query()->where('name->ar' , 'ديانة 1')->first()->id,
            'book_id' => Book::query()->where('name' ,'كتاب 1')->first()->id,
        ]);

//        DB::table('book_type')->insert([
//            'name' => 'masader'
//        ]);

        //  $id =     DB::table('book_type')->where('name' ,'masader')->first()->id;
        //  $id = Book::query()->where('name' ,'كتاب 1')->first()->id;
//        DB::table('book_ranges')->insert(['book_type_id' => $id, 'order' => 1,'type' =>'static', 'number' => '1']);//مجال الكتاب
//        DB::table('book_ranges')->insert(['book_type_id' => $id, 'order' => 2,'type' =>'static', 'number' => '22']);//نوع المعرفة
//        DB::table('book_ranges')->insert(['book_type_id' => $id, 'order' => 3,'type' =>'static', 'number' => '1']);//عدد الوجدات المعرفية
//        DB::table('book_ranges')->insert(['book_type_id' => $id, 'order' => 4,'type' =>'static', 'number' => '001']);//وكيل التسجبل
//        DB::table('book_ranges')->insert(['book_type_id' => $id, 'order' => 5,'type' =>'static', 'number' => '001']);//رقم الكتاب
//        DB::table('book_ranges')->insert(['book_type_id' => $id, 'order' => 6,'type' =>'dynamic', 'number' => '001']);//رقم الوحدة المعرفية

    }
}
