<?php

use App\Models\Publisher;
use App\Models\User;
use Illuminate\Database\Seeder;

class PublisherSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = factory(User::class)->create();
        $publisher = new Publisher(['name' => "دار الفكر","user_id" => $user->id]);
        $publisher->setTranslation('name', 'en', 'Dar Al fiker');
        $publisher->save();

        $user = factory(User::class)->create();
        $publisher = new Publisher(['name' => "دار المعارف","user_id" => $user->id]);
        $publisher->setTranslation('name', 'en', 'Dar Al maharef');
        $publisher->save();
    }
}
