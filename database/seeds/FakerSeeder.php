<?php

use App\KuAccessory;
use Illuminate\Database\Seeder;

class FakerSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(KuAccessory::class,10)->create();
    }
}
