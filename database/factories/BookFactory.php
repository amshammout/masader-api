<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Book;
use App\Models\BookSize;
use App\Models\Category;
use App\Models\CopyType;
use App\Models\CoverType;
use App\Models\Language;
use App\Models\Publisher;
use Faker\Generator as Faker;

use Illuminate\Support\Facades;


$factory->define(Book::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'author' => $faker->name,
        'order' => $faker->numberBetween(0, 20),
        'content_type_number' => $faker->numberBetween(0, 20),
        'ku_range_number' => $faker->numberBetween(0, 20),
        'ku_range' => $faker->numberBetween(0, 20),
        'book_number' => $faker->numberBetween(0, 20),
        'book_identity_number' => $faker->numberBetween(0, 20),
        'publisher_id' => Publisher::query()->count() == 0 ? factory(Publisher::class)->create()->id : Publisher::all()->first()->id,
        'edition_number' => $faker->numberBetween(0, 20),
        'language_id' => Language::query()->count() == 0 ? factory(Language::class)->create()->id : Language::all()->first()->id,
        'pages_count' => $faker->numberBetween(0, 20),
        'folders_count' => $faker->numberBetween(0, 20),
        'isbn' => $faker->numberBetween(0, 20),
        'ikun' => $faker->numberBetween(0, 20),
        'cover_type_id' => CoverType::query()->count() == 0 ? factory(CoverType::class)->create()->id : CoverType::all()->first()->id,
        'book_size_id' => BookSize::query()->count() == 0 ? factory(BookSize::class)->create()->id : BookSize::all()->first()->id,
        'publication_year' => $faker->numberBetween(0, 20),
        'copy_type_id' => CopyType::query()->count() == 0 ? factory(CopyType::class)->create()->id : CopyType::all()->first()->id,
        'summary' => $faker->text(30),
    ];
});
