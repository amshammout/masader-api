<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\KnowledgeUnit;
use App\Model;
use App\Models\Book;
use Faker\Generator as Faker;

$factory->define(KnowledgeUnit::class, function (Faker $faker) {
    return [
        "header" => $faker->text,
        "body" => $faker->text(200),
        "book_id" => factory(Book::class)->create()->id,
        "number" => $faker->randomNumber(4)
    ];
});
