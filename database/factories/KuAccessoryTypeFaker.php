<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\KnowledgeUnit;
use App\Models\KuAccessoryType;
use App\Model;
use App\Models\Book;
use Faker\Generator as Faker;

$factory->define(KuAccessoryType::class, function (Faker $faker) {
    return [
        "name" => $faker->text(7),
    ];
});
