<?php

use App\Models\BookSize;
use App\Models\CopyType;
use App\Models\CoverType;
use App\Models\Language;
use Illuminate\Http\Request;
use Illuminate\Routing\Router;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::group(['namespace' => 'Auth\\',
    'middleware' => ['always-json']
], function (Router $router) {



//    Agent Operations
    $router->post('/agent/registerByEmail', 'AgentApiController@registerByEmail');
    $router->post('/user/loginByEmail', 'UserApiController@loginByEmail');
    $router->post('/user/socialLogin/{provider}', 'AgentApiController@socialLogin');
    $router->post('/user/callBack_socialLogin/{provider}', 'AgentApiController@callBack_socialLogin');
    $router->post('/user/forgetByEmail', 'AgentApiController@forgetByEmail');
    $router->post('/user/resetByEmail', 'AgentApiController@resetByEmail');
    $router->post('/user/refreshToken', 'AgentApiController@refreshToken');
    $router->post('/user/deleteInstanceId', 'AgentApiController@deleteInstanceId');
    $router->group(['middleware' => ['auth:api']],
        function (Router $router) {
            $router->post('/user/reSendSMSVerificationCode', 'AgentApiController@reSendSMSVerificationCode');
            $router->get('/user/canSendSMSVerificationCode', 'AgentApiController@canSendSMSVerificationCode');
            $router->post('/user/reSendMailVerificationCode', 'AgentApiController@reSendMailVerificationCode');
            $router->post('/user/saveInstanceId', 'AgentApiController@saveInstanceId');
            $router->post('/user/verifyEmail', 'AgentApiController@verifyEmail');
            $router->post('/user/saveLocale', 'AgentApiController@saveLocale');
            $router->get('/user/profile', 'AgentApiController@profile');
            $router->put('user/update', 'AgentApiController@update');
        });
});
Route::group(['namespace' => 'Auth\\','middleware' => ['auth:api','always-json','checkIfAdmin']], function (Router $router){
    //    Publisher Operations
    $router->post('/publisher/registerByEmail', 'PublisherApiController@registerByEmail');
//    Admin Operations
    $router->post('/admin/registerByEmail', 'AdminApiController@registerByEmail');
});
Route::group(['namespace' => 'ApiControllers\\'],
    function (Router $router) {
        $router->apiResource('category', 'CategoryApiController');
        $router->get('relationalCategory', 'CategoryApiController@getRelationalCategory');
        $router->apiResource('book', 'BookApiController');
        $router->apiResource('publisher', 'PublisherApiController');
        $router->get('categoryBooks/{id}', 'BookApiController@getCategoryBooks');
        $router->post('addBookCategories/{bookId}', 'BookApiController@addBookCategories');
        $router->get("CopyTypes", "LookUpApiController@getCopyTypes");
        $router->get("CoverTypes", "LookUpApiController@getCoverTypes");
        $router->get("Languages", "LookUpApiController@getLanguages");
        $router->get("BookSizes", "LookUpApiController@getBookSizes");
        $router->post("knowledge_unit/image", "KnowledgeUnitApiController@storeImage");
        $router->apiResource("knowledge_unit", "KnowledgeUnitApiController");
        $router->apiResource("book_publish_request", "BookPublishRequestApiController");
        $router->post("ku_from_excel","KnowledgeUnitApiController@storeFromExcel");
    });
